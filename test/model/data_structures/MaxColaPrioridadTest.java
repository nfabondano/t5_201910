package model.data_structures;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MaxColaPrioridadTest {

	/**
	 * atributo que representa una cola de prioridad de numeros
	 */
	private MaxColaPrioridad<Integer> numeros;
	
	/**
	 * M�todo que incializa la cola de prioridad
	 */
	public void setUpEscenario() {
		numeros = new MaxColaPrioridad<Integer>();
	}
	
	/**
	 * Test que verifica que el m�todo esVacia funcione correctamente
	 */
	@Test
	void esVaciaTest0() {
		setUpEscenario();
		assertTrue("la cola de prioridad deber�a estar vacia", numeros.esVacia());
	}
	
	/**
	 * Test que verifica que el m�todo darNumeroElementos funcione correctamente
	 */
	@Test
	void darNumeroElementosTest0() {
		setUpEscenario();
		assertEquals("la cola no deber�a tener elementos", 0, numeros.darNumeroElementos());
	}
	
	/**
	 * Test que verifica que se esten agregando elementos de manera correcta
	 */
	@Test
	void agregarTest() {
		setUpEscenario();
		numeros.agregar(2);
		assertEquals("el tama�o no es el correcto",1,numeros.darNumeroElementos());
		numeros.agregar(5);
		assertEquals("el tama�o no es el correcto",2,numeros.darNumeroElementos());
	}
	
	/**
	 * Test que verifica que se este devolviendo el elemento maximo de manera correcta
	 */
	@Test
	void maxTest() {
		setUpEscenario();
		numeros.agregar(5);
		numeros.agregar(2);
		int max = numeros.max();
		assertEquals("el maximo no es el correcto",5, max);
		numeros.agregar(10);
		max = numeros.max();
		assertEquals("el maximo no es el correcto",10, max);
		numeros.agregar(4);
		max = numeros.max();
		assertEquals("el maximo no es el correcto",10, max);
	}
	
	/**
	 * Test que verifica que se este borrando el elemento maximo del priority queue
	 */
	@Test
	void deleteMaxTest() {
		setUpEscenario();
		for(int i = 0; i < 10; i++) {
			numeros.agregar(i);
		}
		for(int i = 9; i >= 0;i--) {
			int del = numeros.delMax();
			assertEquals("el maximo no es el correcto",del,i);
		}
		Integer del = numeros.delMax();
		assertNull("el valor deber�a ser nulo",del);
	}
	
	/**
	 * Test que verifica que el m�todo esVacia funcione correctamente
	 */
	@Test
	void esVacioTest1() {
		setUpEscenario();
		numeros.agregar(2);
		assertFalse("la cola no deber�a estar vacia",numeros.esVacia());
		numeros.delMax();
		assertTrue("la cola deber�a estar vac�a",numeros.esVacia());
	}
}
