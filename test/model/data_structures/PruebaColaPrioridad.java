package model.data_structures;
import model.data_structures.OrderedResizingArrayList;
import model.vo.VOMovingViolations;
import model.vo.LocationVO;

import java.util.Comparator;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PruebaColaPrioridad {
	
	/**
	 * atributo que representa una cola de prioridad 
	 */
	private MaxColaPrioridad<LocationVO> infraccionesCola;
	
	/**
	 * atributo que representa una cola de prioridad implementada en un heap
	 */
	private MaxHeapCP<LocationVO> infraccionesHeap;
	
	/**
	 * arreglo dinamico donde se van a guardar los datos
	 */
	private OrderedResizingArrayList<LocationVO> datos;
	
	/**
	 * M�todo que inicializa las implementaciones de priority queues y 
	 * el arreglo donde se van a guardar los datos
	 */
	public void setUpEscenario0() {
		infraccionesCola = new MaxColaPrioridad<LocationVO>();
		infraccionesHeap = new MaxHeapCP<LocationVO>();
		datos = new OrderedResizingArrayList<LocationVO>();
	}
	
	/**
	 * Todos los datos se encuentran de mayor a menor
	 */
	public void setUpEscenario1() {
		setUpEscenario0();
		for(int i = 1000; i >= 0; i--) {
			datos.add(new LocationVO(i,"noImportance",i));
		}
	}
	/**
	 * Todos los datos se encuentran de menor a mayor
	 */
	public void setUpEscenario2() {
		setUpEscenario0();
		for(int i = 0; i <= 1000; i++) {
			datos.add(new LocationVO(i,"noImportance",i));
		}
	}
	
	/**
	 * los datos se guardan de manera aleatoria
	 */
	public void setUpEscenario3() {
		setUpEscenario0();
		for(int i = 0; i <= 1000; i++) {
			int x = (int)(Math.random()*((1000-0)+1))+0;
			datos.add(new LocationVO(x,"noImportance",x));
		}
	}
	
	/**
	 * Test que prueba que ambas estructuras esten vacias cuando se inicializan
	 */
	@Test
	public void esVaciaTest0() {
		setUpEscenario0();
		assertTrue("la cola de prioridad deber�a estar vacia", infraccionesCola.esVacia());
		assertTrue("el heap deber�a estar vacio", infraccionesHeap.esVacia());
	}
	
	/**
	 * Test que verifica que el numero de elementos sea cero en ambas estructuras cuando se 
	 * incializan
	 */
	@Test
	public void darNumeroElementosTest0() {
		setUpEscenario0();
		assertEquals("la cola no deber�a tener elementos", 0, infraccionesCola.darNumeroElementos());
		assertEquals("el heap no deber�a tener elementos",0,infraccionesHeap.darNumeroElementos());
	}
	
	/**
	 * M�todo que verifica que en ambas estructuras se agregen los elementos
	 */
	@Test
	public void agregarTest0() {
		setUpEscenario0();
		infraccionesCola.agregar(new LocationVO(1,"NoImportance",2));
		infraccionesHeap.agregar(new LocationVO(1,"NoImportance",2));
		assertEquals("el tama�o no es el correcto",1,infraccionesCola.darNumeroElementos());
		assertEquals("el tama�o no es el correcto",1,infraccionesHeap.darNumeroElementos());
		infraccionesCola.agregar(new LocationVO(1,"NoImportance",2));
		infraccionesHeap.agregar(new LocationVO(1,"NoImportance",2));
		assertEquals("el tama�o no es el correcto",2,infraccionesCola.darNumeroElementos());
		assertEquals("el tama�o no es el correcto",2,infraccionesHeap.darNumeroElementos());
	}
	
	/**
	 * Test que verifica que las estructuras de datos se esten creando correctamente seg�n su 
	 * elemento maximo
	 */
	@Test
	public void maxTest0() {
		setUpEscenario1();
		for(int i = 0; i < datos.getSize(); i++) {
			infraccionesCola.agregar(datos.getElement(i));
			infraccionesHeap.agregar(datos.getElement(i));
		}
		LocationVO max0 = infraccionesCola.max();
		LocationVO max1 = infraccionesHeap.max();
		LocationVO comparing = new LocationVO(1000,"noImportance",1000);
		assertTrue("los maximos deber�an ser los mismos", max0.compareTo(max1) == 0);
		assertTrue("el maximo no es correcto", max0.compareTo(comparing) == 0);
	}
	
	/**
	 * Test que verifica que las estructuras de datos se esten creando correctamente seg�n su 
	 * elemento maximo
	 */
	@Test
	public void maxTest1() {
		setUpEscenario2();
		for(int i = 0; i < datos.getSize(); i++) {
			infraccionesCola.agregar(datos.getElement(i));
			infraccionesHeap.agregar(datos.getElement(i));
		}
		LocationVO max0 = infraccionesCola.max();
		LocationVO max1 = infraccionesHeap.max();
		LocationVO comparing = new LocationVO(1000,"noImportance",1000);
		assertTrue("los maximos deber�an ser los mismos", max0.compareTo(max1) == 0);
		assertTrue("el maximo no es correcto", max0.compareTo(comparing) == 0);
	}
	
	/**
	 * Test que verifica que las estructuras de datos se esten creando correctamente seg�n su 
	 * elemento maximo
	 */
	@Test
	public void maxTest2() {
		setUpEscenario3();
		for(int i = 0; i < datos.getSize(); i++) {
			infraccionesCola.agregar(datos.getElement(i));
			infraccionesHeap.agregar(datos.getElement(i));
		}
		LocationVO max0 = infraccionesCola.max();
		LocationVO max1 = infraccionesHeap.max();
		assertTrue("los maximos deber�an ser los mismos", max0.compareTo(max1) == 0);
	}
	
	/**
	 * Test que verifica que se borren correctamente los elementos maximos de ambas 
	 * estructuras de datos.
	 */
	@Test
	public void deleteMaxTest0() {
		setUpEscenario1();
		for(int i = 0; i < datos.getSize(); i++) {
			infraccionesCola.agregar(datos.getElement(i));
			infraccionesHeap.agregar(datos.getElement(i));
		}
		assertTrue("el tama�o deber�an ser los mismo", infraccionesCola.darNumeroElementos() == infraccionesHeap.darNumeroElementos());
		LocationVO max0 = infraccionesCola.delMax();
		LocationVO max1 = infraccionesHeap.delMax();
		assertTrue("los maximos deber�an ser los mismos", max0.compareTo(max1) == 0);
		while(infraccionesCola.max() != null) {
			LocationVO max2 = infraccionesCola.delMax();
			LocationVO max3 = infraccionesHeap.delMax();
			assertTrue("los maximos deber�an ser los mismos", max2.compareTo(max3) == 0);
			assertTrue("la cola de prioridad no esta bien contruida", max0.compareTo(max2) >= 0);
			assertTrue("el heap no esta bien construido",max1.compareTo(max3) >= 0);
			max0 = max2;
			max1 = max3;
		}
	}
	
	/**
	 * Test que verifica que se borren correctamente los elementos maximos de ambas 
	 * estructuras de datos.
	 */
	@Test
	public void deleteMaxTest1() {
		setUpEscenario2();
		for(int i = 0; i < datos.getSize(); i++) {
			infraccionesCola.agregar(datos.getElement(i));
			infraccionesHeap.agregar(datos.getElement(i));
		}
		assertTrue("el tama�o deber�an ser los mismo", infraccionesCola.darNumeroElementos() == infraccionesHeap.darNumeroElementos());
		LocationVO max0 = infraccionesCola.delMax();
		LocationVO max1 = infraccionesHeap.delMax();
		assertTrue("los maximos deber�an ser los mismos", max0.compareTo(max1) == 0);
		while(infraccionesCola.max() != null) {
			LocationVO max2 = infraccionesCola.delMax();
			LocationVO max3 = infraccionesHeap.delMax();
			assertTrue("los maximos deber�an ser los mismos", max2.compareTo(max3) == 0);
			assertTrue("la cola de prioridad no esta bien contruida", max0.compareTo(max2) >= 0);
			assertTrue("el heap no esta bien construido",max1.compareTo(max3) >= 0);
			max0 = max2;
			max1 = max3;
		}
	}
	
	/**
	 * Test que verifica que se borren correctamente los elementos maximos de ambas 
	 * estructuras de datos.
	 */
	@Test
	public void deleteMaxTest2() {
		setUpEscenario3();
		for(int i = 0; i < datos.getSize(); i++) {
			infraccionesCola.agregar(datos.getElement(i));
			infraccionesHeap.agregar(datos.getElement(i));
		}
		assertTrue("el tama�o deber�an ser los mismo", infraccionesCola.darNumeroElementos() == infraccionesHeap.darNumeroElementos());
		LocationVO max0 = infraccionesCola.delMax();
		LocationVO max1 = infraccionesHeap.delMax();
		assertTrue("los maximos deber�an ser los mismos", max0.compareTo(max1) == 0);
		while(infraccionesCola.max() != null) {
			LocationVO max2 = infraccionesCola.delMax();
			LocationVO max3 = infraccionesHeap.delMax();
			assertTrue("el elemento no se encuentra realmente en el arreglo",checkElement(max2));
			assertTrue("los maximos deber�an ser los mismos", max2.compareTo(max3) == 0);
			assertTrue("la cola de prioridad no esta bien contruida", max0.compareTo(max2) >= 0);
			assertTrue("el heap no esta bien construido",max1.compareTo(max3) >= 0);
			max0 = max2;
			max1 = max3;
		}
	}
	
	/**
	 * M�todo que verifica que el elemento que se acaba de borrar exista en los datos
	 * @param lo elemento que se borro
	 * @return booleano indicando si el elemento si esta o no
	 */
	private boolean checkElement(LocationVO lo) {
		for(int i = 0; i < datos.getSize(); i++) {
			if(datos.getElement(i).compareTo(lo) == 0)
				return true;
		}
		return false;
	} 
}
