package controller;

import java.io.File;
import model.vo.LocationVO;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Scanner;

import com.opencsv.CSVReader;

import model.data_structures.IColaPrioridad;
import model.data_structures.IOrderedResizingArrayList;
import model.data_structures.MaxColaPrioridad;
import model.data_structures.MaxHeapCP;
import model.data_structures.OrderedResizingArrayList;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller {

	/**
	 * Constante del archivo de enero.
	 */
	public final static String ENERO = "./data/Moving_Violations_Issued_In_January_2018.csv";

	/**
	 * Constante del archivo de febrero.
	 */
	public final static String FEBRERO = "./data/Moving_Violations_Issued_In_February_2018.csv";

	/**
	 * Constante del archivo de marzo.
	 */
	public final static String MARZO = "./data/Moving_Violations_Issued_In_March_2018.csv";

	/**
	 * Constante del archivo de abril.
	 */
	public final static String ABRIL = "./data/Moving_Violations_Issued_In_April_2018.csv";


	/**
	 * forma de conectarse con la clase que imprime informaci�n a la consola
	 */
	private MovingViolationsManagerView view;

	/**
	 * Lista donde se van a cargar los datos de los archivos
	 */
	private IOrderedResizingArrayList<VOMovingViolations> movingViolationsArrayOrdered;	

	private IColaPrioridad<LocationVO> locationsVOCola;

	private IColaPrioridad<LocationVO> locationsVOHeap;

	/**
	 * Archivos para leer en csv.
	 */
	private String[] csvData;

	/**
	 * Comparador por fecha.
	 */
	private Comparator<VOMovingViolations> comparadorFecha;

	/**
	 * Comparador por objectId.
	 */
	private Comparator<VOMovingViolations> comparadorObject;

	/**
	 * Comparador por violation code.
	 */
	private Comparator<VOMovingViolations> comparadorCode;

	/**
	 * Comparador por hora.
	 */
	private Comparator<VOMovingViolations> comparadorHora;

	/**
	 * Comparador por violation description.
	 */
	private Comparator<VOMovingViolations> comparadorDesc;

	/**
	 * Comparador por streetSegidIdInvertido
	 */
	private Comparator<VOMovingViolations> comparadorStreetSegId;

	/**
	 * comparador por streetID
	 */
	private Comparator<VOMovingViolations> comparadorStreetId;

	/**
	 * Muestra del controller
	 */
	private OrderedResizingArrayList<VOMovingViolations> muestra;

	/**
	 * Tama�o del arreglo
	 */
	private int size;

	/**
	 * Constructor del controlador
	 */
	public Controller() {
		view = new MovingViolationsManagerView();
		csvData = new String[12];
		csvData[0] = ENERO; 
		csvData[1] = FEBRERO;
		csvData[2] = MARZO;
		csvData[3] = ABRIL;
		comparadorFecha = new VOMovingViolations.ComparatorDate();
		comparadorObject = new VOMovingViolations.ComparatorObjectID();
		comparadorCode = new VOMovingViolations.ComparatorViolationCode();
		comparadorDesc = new VOMovingViolations.ComparatorViolationDesc();
		comparadorHora = new VOMovingViolations.ComparatorHour();
		comparadorStreetSegId = new VOMovingViolations.ComparadorStreetSegId();
		comparadorStreetId = new VOMovingViolations.ComparadorStreetId();
	}

	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 0:
				loadMovingViolations();
				break;
			case 1:
				view.printTabla();
				for(int i = 0; i<8; i++) {
					int size = 50000*i;
					double[] promAgregar = this.generarMuestra(size);
					double[] promDel = this.promedioDelMax();
					view.printProm(promAgregar, promDel, size);
				}
				break;
			case 2:	
				view.printMessage("Ingrese la fecha con hora inicial (Ej : 2017-03-28T15:30:20)");
				LocalDateTime fInicial = convertirFecha_Hora_LDT(sc.next()+".000Z");

				view.printMessage("Ingrese la fecha con hora final (Ej : 2017-03-28T15:30:20)");
				LocalDateTime fFinal = convertirFecha_Hora_LDT(sc.next() + ".000Z");
				
				double time1 = 0; double time2 = 0;
				double startTime = 0; double endTime = 0;
				
				startTime = System.currentTimeMillis();
				this.crearMaxHeap(fInicial, fFinal);
				endTime = System.currentTimeMillis();
				time1 = (endTime - startTime);
				
				startTime = System.currentTimeMillis();
				this.crearMaxColaP(fInicial, fFinal);
				endTime = System.currentTimeMillis();
				time2 = (endTime - startTime);
				
				view.printWashington(time1, time2);
				break;
			case 3:	
				fin=true;
				sc.close();
				break;
			}
		}

	}

	/**
	 * M�todo que carga las infracciones
	 */
	public void loadMovingViolations() {
		movingViolationsArrayOrdered = new OrderedResizingArrayList<VOMovingViolations>(size);
		try {
			for(int i = 0; i<4; i++) {
				File m = new File(csvData[i]);
				FileReader nm = new FileReader(m);
				CSVReader reader = new CSVReader(nm);
				String nextLine[];
				nextLine = reader.readNext();
				while((nextLine = reader.readNext()) != null) {
					VOMovingViolations a�adir = new VOMovingViolations(nextLine);
					//movingViolationsArray.add(a�adir);
					movingViolationsArrayOrdered.add(a�adir);
				}
				reader.close();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Generar una muestra aleatoria de tamaNo n de los datos leidos.
	 * Los datos de la muestra se obtienen de las infracciones guardadas en la Estructura de Datos.
	 * @param n tamaNo de la muestra, n > 0
	 * @return muestra generada
	 */
	public double[] generarMuestra( int n ) {
		muestra = new OrderedResizingArrayList<VOMovingViolations>();
		locationsVOCola = new MaxColaPrioridad<LocationVO>();
		locationsVOHeap = new MaxHeapCP<LocationVO>();
		double[] promedios = new double[2];
		for (int i = 0; i<n; i++) {
			int in = (int) (Math.random()*n);
			muestra.add(movingViolationsArrayOrdered.getElement(in));
		}
		muestra.sort(comparadorStreetId);
		int size = muestra.getSize();
		if(size == 0) {
			promedios[0] = 0.0; promedios[1] = 0.0; return promedios;
		}
		String lastId = muestra.getElement(0).getAdressID();
		int total = 1;
		double timeHeap = 0; double timeCola = 0;
		double startTime = 0; double endTime = 0;
		for(int i = 1; i < size; i++) {
			if(!muestra.getElement(i).getAdressID().equals(lastId)) {
				LocationVO location = new LocationVO(0,muestra.getElement(i-1).getLocation(),total);
				try {
					location = new LocationVO(Integer.parseInt(lastId),muestra.getElement(i-1).getLocation(),total);
				}
				catch (Exception e){
					location = new LocationVO(0,muestra.getElement(i-1).getLocation(),total);
				}
				startTime = System.currentTimeMillis();
				locationsVOHeap.agregar(location);
				endTime = System.currentTimeMillis();
				timeHeap += (endTime - startTime);

				startTime = System.currentTimeMillis();
				locationsVOCola.agregar(location);
				endTime = System.currentTimeMillis();
				timeCola += (endTime - startTime);

				lastId = movingViolationsArrayOrdered.getElement(i).getAdressID();
				total = 1;
			}
			else {
				total++;
			}
		}
		LocationVO location = new LocationVO(0,movingViolationsArrayOrdered.getElement(size-1).getLocation(),total);
		try {
			location = new LocationVO(Integer.parseInt(lastId),movingViolationsArrayOrdered.getElement(size-1).getLocation(),total);
		}
		catch (Exception e){
			location = new LocationVO(0,movingViolationsArrayOrdered.getElement(size-1).getLocation(),total);
		}		
		
		startTime = System.currentTimeMillis();
		locationsVOHeap.agregar(location);
		endTime = System.currentTimeMillis();
		timeHeap += (endTime - startTime);

		startTime = System.currentTimeMillis();
		locationsVOCola.agregar(location);
		endTime = System.currentTimeMillis();
		timeCola += (endTime - startTime);

		promedios[0] = timeHeap/size;
		promedios[1] = timeCola/size;

		return promedios;
	}

	/**
	 * M�todo que calcula los promedios de tiempo de borrar el maximo elemento en 
	 * las dos implementaciones de priority Queue
	 * @return un arreglo con los datos de tiempo
	 */
	public double[] promedioDelMax() {
		double[] promedios = new double[2];

		double timeHeap = 0; double timeCola = 0;
		double startTime = 0; double endTime = 0;
		int size = locationsVOHeap.darNumeroElementos();
		if(size == 0) {
			promedios[0] = 0.0; promedios[1] = 0.0; return promedios;
		}
		
		for (int i = 0; i < size; i++) {

			startTime = System.currentTimeMillis();
			locationsVOHeap.delMax();
			endTime = System.currentTimeMillis();
			timeHeap += (endTime - startTime);

			startTime = System.currentTimeMillis();
			locationsVOCola.delMax();
			endTime = System.currentTimeMillis();
			timeCola += (endTime - startTime);
		}
		
		promedios[0] = timeHeap/size;
		promedios[1] = timeCola/size;

		return promedios;
	}

	/**
	 * M�todo que crea una cola de prioridad con las fechas finales y iniciales pasadas por parametro
	 * @param fInicial fecha inicial de las infracciones
	 * @param fFinal fecha final de las infracciones
	 * @return la cola de prioridad creada
	 */
	public IColaPrioridad<LocationVO> crearMaxColaP(LocalDateTime fInicial, LocalDateTime fFinal ) {
		locationsVOCola = new MaxColaPrioridad<LocationVO>();

		movingViolationsArrayOrdered.sort(comparadorStreetId);
		int size = movingViolationsArrayOrdered.getSize();
		if(size == 0) return null;

		String lastId = movingViolationsArrayOrdered.getElement(0).getAdressID();
		int total = 1;
		
		for(int i = 1; i < size; i++) {
			VOMovingViolations actual = movingViolationsArrayOrdered.getElement(i);
			if(actual.getFecha().isAfter(fInicial) && actual.getFecha().isBefore(fFinal) )
				if(!actual.getAdressID().equals(lastId)) {
					LocationVO location = new LocationVO(0,movingViolationsArrayOrdered.getElement(i-1).getLocation(),total);
					try {
						location = new LocationVO(Integer.parseInt(lastId),movingViolationsArrayOrdered.getElement(i-1).getLocation(),total);
					}
					catch (Exception e){
						location = new LocationVO(0,movingViolationsArrayOrdered.getElement(i-1).getLocation(),total);
					}
					locationsVOHeap.agregar(location);
					

					lastId = movingViolationsArrayOrdered.getElement(i).getAdressID();
					total = 1;
				}
				else {
					total++;
				}
		}
		
		LocationVO location = new LocationVO(0,movingViolationsArrayOrdered.getElement(size-1).getLocation(),total);
		try {
			location = new LocationVO(Integer.parseInt(lastId),movingViolationsArrayOrdered.getElement(size-1).getLocation(),total);
		}
		catch (Exception e){
			location = new LocationVO(0,movingViolationsArrayOrdered.getElement(size-1).getLocation(),total);
		}		
		locationsVOCola.agregar(location);

		return locationsVOCola;
	}
	
	/**
	 * Crea una priority queue a partir de un queue con las fechas iniciales y finales para las infracciones
	 * @param fInicial fecha inicial de las infracciones
	 * @param fFinal fecha final de las infracciones
	 * @return el priority queue con las infracciones por fecha inicial y final
	 */
	public IColaPrioridad<LocationVO> crearMaxHeap(LocalDateTime fInicial, LocalDateTime fFinal ) {
		locationsVOHeap = new MaxHeapCP<LocationVO>();

		movingViolationsArrayOrdered.sort(comparadorStreetId);
		int size = movingViolationsArrayOrdered.getSize();
		if(size == 0) return null;

		String lastId = movingViolationsArrayOrdered.getElement(0).getAdressID();
		int total = 1;
		
		for(int i = 1; i < size; i++) {
			VOMovingViolations actual = movingViolationsArrayOrdered.getElement(i);
			if(actual.getFecha().isAfter(fInicial) && actual.getFecha().isBefore(fFinal) )
				if(!actual.getAdressID().equals(lastId)) {
					LocationVO location = new LocationVO(0,movingViolationsArrayOrdered.getElement(i-1).getLocation(),total);
					try {
						location = new LocationVO(Integer.parseInt(lastId),movingViolationsArrayOrdered.getElement(i-1).getLocation(),total);
					}
					catch (Exception e){
						location = new LocationVO(0,movingViolationsArrayOrdered.getElement(i-1).getLocation(),total);
					}
					locationsVOHeap.agregar(location);
					

					lastId = movingViolationsArrayOrdered.getElement(i).getAdressID();
					total = 1;
				}
				else {
					total++;
				}
		}
		
		LocationVO location = new LocationVO(0,movingViolationsArrayOrdered.getElement(size-1).getLocation(),total);
		try {
			location = new LocationVO(Integer.parseInt(lastId),movingViolationsArrayOrdered.getElement(size-1).getLocation(),total);
		}
		catch (Exception e){
			location = new LocationVO(0,movingViolationsArrayOrdered.getElement(size-1).getLocation(),total);
		}		
		locationsVOHeap.agregar(location);

		return locationsVOHeap;
	}

	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}


	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora) {
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));
	}
}
