package model.vo;

public class LocationVO implements Comparable<LocationVO>{
	
	//-----------------------------Atributos-----------------------------//
	
	/**
	 * Atributo que representa el id de la dirreci�n
	 */
	private int adressId;
	
	/**
	 * localizaci�n escrita que representa el id
	 */
	private String location;
	
	/**
	 * numero de infracciones que se dieron en el mismo addressId
	 */
	private int numberOfRegisters;
	
	//---------------------------------Constructores------------------------//
	
	
	/**
	 * iniciliza el objeto con el addressId, location y numberOfRegisters pasados por parametro
	 * @param pAdressId numero que identifica la localizaci�n
	 * @param pLocation la localizaci�n que identifica el addressID pLocation != null
	 * @param pNumberOfRegisters numero de infracciones que se dieron en el addressID. pNumberOfRegisters > 0
	 */
	public LocationVO(int pAdressId, String pLocation, int pNumberOfRegisters) {
		adressId = pAdressId;
		location = pLocation;
		numberOfRegisters = pNumberOfRegisters;
	}
	
	
	//--------------------------------M�todos--------------------------------//
	
	/**
	 * M�todo que devuelve el addressID
	 * @return el AddressID
	 */
	public int getAdrresId() {
		return adressId;
	}
	
	/**
	 * M�todo que devuelve el location
	 * @return location
	 */
	public String getLocation() {
		return location;
	}
	
	/**
	 * m�todo que devuelve el numberOfRegisters
	 * @return numberOfRegisters
	 */
	public int getNumberOfRegisters() {
		return numberOfRegisters;
	}

	/**
	 * Comparador natural de LocationVO
	 * Compara seg�n el numero de registros y localizaci�n en caso de ser necesario
	 * @return un numero que representa si el mayor, menor o igual a otro LocationVO
	 */
	public int compareTo(LocationVO o) {
		if(numberOfRegisters > o.numberOfRegisters)
			return 1;
		else if(numberOfRegisters < o.numberOfRegisters)
			return -1;
		else
			return location.compareTo(o.location);
	}
}
