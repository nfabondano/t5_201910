package model.data_structures;

public interface IColaPrioridad<T extends Comparable<T>> {
	
	public int darNumeroElementos();
	
	public void agregar(T elemento);
	
	public T delMax();
	
	public T max();
	
	public boolean esVacia(); 
}
