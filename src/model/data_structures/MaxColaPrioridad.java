package model.data_structures;

public class MaxColaPrioridad <T extends Comparable<T>> implements IColaPrioridad<T>{
	/**
	 * 
	 */
	private int numeroElementos; 
	
	/**
	 * 
	 */
	private Node first;
	
	/**
	 * 
	 */
	private Node last;
	
	/**
	 * 
	 *
	 */
	class Node{
		/**
		 * 
		 */
		Node next;
		
		/**
		 * 
		 */
		Node prev;
		/**
		 * 
		 */
		T item;
		
		/**
		 * 
		 * @param pNext
		 * @param pPrev
		 * @param pItem
		 */
		public Node(Node pNext, Node pPrev, T pItem){next = pNext;prev = pPrev; item = pItem;}
	}
	
	/**
	 * 
	 */
	public MaxColaPrioridad(){
		numeroElementos = 0;
		first = null;
		last = null;
	}
	
	/**
	 * 
	 * @param newOne
	 * @param last
	 */
	private void changePrevious(Node newOne, Node plast){
		Node oldPrev = plast.prev;
		oldPrev.next = newOne;
		newOne.prev = oldPrev;
		newOne.next = plast;
		plast.prev = newOne;
	}
	
	/**
	 * 
	 * @param newOne
	 * @param previous
	 */
	/*private void changeNext(Node newOne, Node previous){
		Node oldnext = previous.next;
		oldnext.prev = newOne;
		newOne.next = oldnext;
		newOne.prev = previous;
		previous.next = newOne;
	}*/
	
	/**
	 * 
	 * @param elemento
	 */
	private void posicionCorrecta(T elemento){
		Node temp = first;
		while(temp.next != null){
			if(elemento.compareTo(temp.next.item) >= 0){
				//System.out.println("antes de: " + temp.toString());
				//System.out.println("el elemento: " + elemento.toString());
				changePrevious(new Node(null,null,elemento), temp.next);
				break;
			}
			temp = temp.next;
		}
	}
	
	/**
	 * 
	 * @param elemento
	 */
	public void agregar(T elemento){
		if(first == null){
			first = new Node(null,null,elemento);
			last = first;
		}
		else if(elemento.compareTo(first.item) >= 0){
			Node newFirst = new Node(first,null,elemento);
			first.prev = newFirst;
			first = newFirst;//System.out.println("antes de: " + first.toString());
			//System.out.println("el elemento: " + elemento.toString());
			//System.out.println(first.next + "el elemento es:" + elemento.toString());
		}
		else if(elemento.compareTo(last.item) < 0){
		//System.out.println("Pasa por aqui super W");
			//System.out.println("despues de: " + last.toString());
			//System.out.println("el elemento: " + elemento.toString());
			Node newLast = new Node(null,last,elemento);
			last.next = newLast;
			last = newLast;
		}
		else{
			posicionCorrecta(elemento);
		}
	numeroElementos++;	
	}
	
	/**
	 * 
	 * @return
	 */
	public T delMax() {
		if(first == null) return null;
		T answer = first.item;
		//System.out.println("el siguiente es: "+ first.next);
		first = first.next;
		if(first != null) {
			//System.out.println("lelele");
			first.prev = null;
		}
		numeroElementos--;
		return answer;
	}
	
	/**
	 * 
	 * @return
	 */
	public T max() {
		if(first == null) {
			System.out.println("por aqui");
			return null;
		}
		return first.item;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean esVacia() {
		return numeroElementos == 0;
	}

	/**
	 * 
	 */
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return numeroElementos;
	}

}
