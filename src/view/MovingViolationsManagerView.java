package view;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import controller.Controller;
import model.vo.VOMovingViolations;

public class MovingViolationsManagerView 
{
	/**
	 * Constante con el nÃºmero maximo de datos maximo que se deben imprimir en consola
	 */
	public static final int N = 20;

	public void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 5----------------------");
		System.out.println("0. Cargar datos del primer cuatrimestre");
		System.out.println("1. Realizar tabla con promedios de los métodos agregar y delMax para distintos volúmenes de datos");
		System.out.println("2. Consultar tiempo de creación de heap y cola para el depto de transporte de Washington");
		System.out.println("3. Salir");
		System.out.println("Digite el nï¿½mero de opciï¿½n para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}

	public void printMessage(String mensaje) {
		System.out.println(mensaje);
	}

	public void printTabla() {
		System.out.println("FORMATO:");
		System.out.println("- Tamaño muestra: XXX");
		System.out.println("- Tiempo promedio Agregar (milisegundos): MaxHeap - MaxColaPrioridad");
		System.out.println("- Tiempo promedio DelMax (milisegundos): MaxHeap - MaxColaPrioridad");
		System.out.println();
	}
	
	public void printProm(double[] promAgregar, double[] promDel, int size) {
		System.out.println("- Tamaño muestra: "+ size);
		System.out.println("- Tiempo promedio Agregar (milisegundos): " + promAgregar[0] + " - " + promAgregar[1]);
		System.out.println("- Tiempo promedio DelMax (milisegundos): " + promDel[0] + " - " + promDel[1]);
		System.out.println();

	}
	
	public void printWashington(double tiempoHeap, double tiempoCola) {
		System.out.println("Tiempo para crear el heap (milisegundos): " + tiempoHeap);
		System.out.println("Tiempo para crear el cola (milisegundos): " + tiempoCola);
	}
}
